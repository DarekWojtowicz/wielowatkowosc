public class Main {

    public static void main(String[] args) {
        Thread t1 = new Thread(new Counter1s());
        Thread t2 = new Thread(new Counter3s());
        t1.start();
        t2.start();
        t1.interrupt();

    }
}
