package Orbits;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

public class OrbitsApp {
    public static void main(String[] args) {
        Map<String, Integer> objectWitchOrbits = new HashMap<>();
        List<String> santaFromSun = new ArrayList<>();
        List<String> meFromSun = new ArrayList<>();
        List<String> uniqueForMeAndSanta = new ArrayList<>();
        int totalOrbits = 0;
        List<String> dane = new ArrayList<>();
        File file = new File("C:\\Projects\\wielowatkowosc\\src\\Orbits\\orbits.txt");
      //  File file = new File("C:\\Projects\\wielowatkowosc\\src\\Orbits\\test.txt");
        String line;
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                dane.add(line);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Dane z pliku. Ilość: " + dane.size());
        List<Planet> planetList;
        planetList = dane.stream()
                .map(d -> {
                    String a = d.substring(0, 3);
                    String b = d.substring((4));
                    return new Planet(a, b);
                })
                .collect(Collectors.toList());

        // System.out.println(planetList.size());

        List<String> onlyPlanets = new ArrayList<>();
        List<String> onlySatelits = new ArrayList<>();

        onlyPlanets = planetList.stream()
                .map(p -> p.getPlaneta())
                .collect(Collectors.toList());

        onlySatelits = planetList.stream()
                .map(s -> s.getSatelita())
                .collect(Collectors.toList());

        //   planetList.forEach(s -> System.out.println(s.toString()));

        List<Planet> listOfEnds;
        listOfEnds = new ArrayList<>();

        String satelita;


        for (Planet p : planetList) {
            satelita = p.getSatelita();
            if (!onlyPlanets.contains(satelita)) {
                listOfEnds.add(p);
            }
        }
        System.out.println("Lista koncowych obiektów: ");
        listOfEnds.forEach(System.out::println);

        // liczenie orbit

        Stack<String> branchStack = new Stack<String>();
        int branchCount;

        Planet tmp = null;
        for (Planet p : listOfEnds) {
            branchStack.clear();
            branchCount = 1;
            System.out.println("Analiza z punktu: " + p);
            do {
                for (Planet all : planetList) {
                    if (all.getSatelita().equals(p.getPlaneta())) {
                        System.out.println(p);
                        branchStack.push(p.getSatelita());
                        p = all;
                        branchCount++;
                    }
                }
            } while (!p.getPlaneta().equals("COM"));
            branchStack.push(p.getSatelita());
            System.out.println("Branch counter: " + branchCount);
            System.out.println(branchStack.toString());
            if (branchStack.contains("YOU")) {
                meFromSun = new ArrayList<>(branchStack);
            }
            if (branchStack.contains("SAN")) {
                santaFromSun = new ArrayList<>(branchStack);
            }
            //Do obiektow dopasowujemy orbity
            for (int i = 1; i <= branchCount; i++) {
                objectWitchOrbits.put(branchStack.pop(), i);
            }
        }
        //      System.out.println("Mapa");
        for (Map.Entry<String, Integer> entry : objectWitchOrbits.entrySet()) {
            totalOrbits += entry.getValue();
            // System.out.println(entry.getKey() + " " + entry.getValue());
        }
        System.out.println("Ilość orbit: " + totalOrbits);
        System.out.println();

        System.out.println("Me: ");
        System.out.println(meFromSun.size());
        meFromSun.forEach(System.out::println);
        System.out.println("Santa: ");
        System.out.println(santaFromSun.size());
        santaFromSun.forEach(System.out::println);
        final List<String> fSanta = new ArrayList<>(santaFromSun);
        final List<String> fMe = new ArrayList<>(meFromSun);

        uniqueForMeAndSanta.addAll(
                meFromSun.stream()
                        .filter(s -> !fSanta.contains(s))
                        .collect(Collectors.toList()));

        uniqueForMeAndSanta.addAll(
                santaFromSun.stream()
                        .filter(s -> !fMe.contains(s))
                        .collect(Collectors.toList()));

//        System.out.println("Unikalne: ");
//        uniqueForMeAndSanta.forEach(System.out::println);
        System.out.println("Odległość: " + (uniqueForMeAndSanta.size() - 2));
    }
}
