package Orbits;

public class Planet {
    String planeta;
    String satelita;

    public Planet(String palneta, String satelita) {
        this.planeta = palneta;
        this.satelita = satelita;
    }

    public Planet() {
    }

    public String getPlaneta() {
        return planeta;
    }

    public void setPlaneta(String palneta) {
        this.planeta = palneta;
    }

    public String getSatelita() {
        return satelita;
    }

    public void setSatelita(String satelita) {
        this.satelita = satelita;
    }

    @Override
    public String toString() {
        return "Planet{" +
                "planeta='" + planeta + '\'' +
                ", satelita='" + satelita + '\'' +
                '}';
    }
}
