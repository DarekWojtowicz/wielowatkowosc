import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Double {
    public static void main(String[] args) {
        int a1, a2, a3, a4, a5, a6;
        int start = 153517;
        int stop = 630395;
        int size = stop - start + 1;
        int[] tab = new int[size];
        List<Integer> list = new ArrayList<>();
        String sv;

        for (int i = 0; i < size; i++) {
            tab[i] = start;
            sv = String.valueOf(start);
            Integer[] doubles = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            for (int j = 0; j < 6; j++) {
                if (sv.charAt(j) - 48 == 0) {
                    doubles[0]++;
                }
                if (sv.charAt(j) - 48 == 1) {
                    doubles[1]++;
                }
                if (sv.charAt(j) - 48 == 2) {
                    doubles[2]++;
                }
                if (sv.charAt(j) - 48 == 3) {
                    doubles[3]++;
                }
                if (sv.charAt(j) - 48 == 4) {
                    doubles[4]++;
                }
                if (sv.charAt(j) - 48 == 5) {
                    doubles[5]++;
                }
                if (sv.charAt(j) - 48 == 6) {
                    doubles[6]++;
                }
                if (sv.charAt(j) - 48 == 7) {
                    doubles[7]++;
                }
                if (sv.charAt(j) - 48 == 8) {
                    doubles[8]++;
                }
                if (sv.charAt(j) - 48 == 9) {
                    doubles[9]++;
                }
            }
            // System.out.println(Arrays.toString(doubles));
            List<Integer> doubleList = new ArrayList<Integer>(Arrays.asList(doubles));

            if (sv.charAt(0) <= sv.charAt(1) && sv.charAt(1) <= sv.charAt(2) && sv.charAt(2) <= sv.charAt(3) && sv.charAt(3) <= sv.charAt(4) && sv.charAt(4) <= sv.charAt(5)) {
                if (doubleList.contains(2)) {
                    list.add(start);
                }
            }
            start++;
        }
        System.out.println(tab.length);
        System.out.println(list.size());
        System.out.println(Arrays.asList(list));
    }
}
