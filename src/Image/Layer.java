package Image;

public class Layer {
    int zeros;
    int ones;
    int twos;
    String image;


    public Layer(String image) {
        this.image = image;
        for(int i = 0; i < image.length(); i++ ){
            if(image.charAt(i) == '0'){
                zeros++;
            }
            if(image.charAt(i) == '1'){
                ones++;
            }
            if(image.charAt(i) == '2'){
                twos++;
            }
        }
    }

    public int getZeros() {
        return zeros;
    }

    public void setZeros(int zeros) {
        this.zeros = zeros;
    }

    public int getOnes() {
        return ones;
    }

    public void setOnes(int ones) {
        this.ones = ones;
    }

    public int getTwos() {
        return twos;
    }

    public void setTwos(int twos) {
        this.twos = twos;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Layer{" +
                " image='" + image + '\'' +
                '}';
    }
}
