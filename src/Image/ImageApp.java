package Image;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class ImageApp {
    public static void main(String[] args) {
        int layerSize = 150;
        List<Layer> layerList = new ArrayList<>();
        File file = new File("C:\\Projects\\wielowatkowosc\\src\\Image\\data.txt");
        //    File file = new File("C:\\Projects\\wielowatkowosc\\src\\Image\\test.txt");
        String line = "";
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                System.out.println(line);
                //dane.add(line);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String layerString;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < line.length(); i += layerSize) {
            layerString = line.substring(i, i + layerSize);
            layerList.add(new Layer(layerString));
        }
        System.out.println("Layers przed sortowaniem: " + layerList.size());
        layerList.forEach(System.out::println);

        System.out.println();

//        layerList.sort(Comparator.comparing(Layer::getZeros));
//        System.out.println("Layers po sortowaniu: " + layerList.size());
//        layerList.forEach(System.out::println);

//        System.out.println("Wynik: " + (layerList.get(0).getTwos() * layerList.get(0).getOnes()));

        char[] znaki = new char[layerSize];
        for (int i = 0; i < layerSize; i++){
            znaki[i] = '2';
        }
        for (Layer l : layerList) {
            for (int i = 0; i < layerSize; i++) {
                if ((l.getImage().charAt(i) == '0') && (znaki[i] == '2')) {
                    znaki[i] = '0';
                }
                if (l.getImage().charAt(i) == '1' && (znaki[i] == '2')) {
                    znaki[i] = '1';
                }
//                if (l.getImage().charAt(i) == '2') {
//                    continue;
//                }
            }
        }
        System.out.println("Zdekodowoany obraz: " + znaki.length);
        int k = 0;
        for(char c : znaki){
            System.out.print(c);
            k++;
            if(k%25 == 0){
                System.out.println();
            }
        }
    }
}
