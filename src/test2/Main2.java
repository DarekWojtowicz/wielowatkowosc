package test2;

public class Main2 {

    public static void main(String[] args) {
        Table table = new Table();
        Table table1 = new Table();
        ActionThread1 actionThread1 = new ActionThread1("Base 5", table);
        ActionThread2 actionThread2 = new ActionThread2("Base 100", table1);
        ActionThread3 actionThread3 = new ActionThread3("Base 200", table);
        ActionThread3 actionThread4 = new ActionThread3("Base 750", table1);
        actionThread1.start();
        actionThread2.start();
        actionThread3.start();
        actionThread4.start();
    }
}
