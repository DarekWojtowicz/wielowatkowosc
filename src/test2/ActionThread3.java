package test2;

public class ActionThread3 extends Thread{

    private final Table table;

    public ActionThread3(String name, Table table) {
        super(name);
        this.table = table;
    }

    @Override
    public void run() {
        table.printTable(200);
    }
}
