package test2;

public class ActionThread1 extends Thread {

    private final Table table;

    public ActionThread1(String name, Table table) {
        super(name);
        this.table = table;
    }

    @Override
    public void run() {
        table.printTable(5);
    }
}
