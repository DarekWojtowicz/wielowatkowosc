package test2;

public class ActionThread4 extends Thread{

    private final Table table;

    public ActionThread4(String name, Table table) {
        super(name);
        this.table = table;
    }

    @Override
    public void run() {
        table.printTable(750);
    }
}
