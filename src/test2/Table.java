package test2;

public class Table {

    synchronized void printTable(int n) {//method not synchronized
        for (int i = 1; i <= 5; i++) {
            System.out.printf("%d, %s\n", n * (i * 2), Thread.currentThread().getName());
            try {
                Thread.sleep(300);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
}