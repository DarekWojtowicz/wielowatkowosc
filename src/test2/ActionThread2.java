package test2;

public class ActionThread2 extends Thread {

    private final Table table;

    public ActionThread2(String name, Table table) {
        super(name);
        this.table = table;
    }

    @Override
    public void run() {
        table.printTable(100);
    }
}
