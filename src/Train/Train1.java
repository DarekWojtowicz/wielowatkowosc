package Train;

public class Train1 extends Thread{

    Station station;
    public Train1(Station station){
        this.station = station;
    }

    @Override
    public void run() {
        station.information();
    }
}
