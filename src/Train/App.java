package Train;

public class App {
    public static void main(String[] args) {
        Station station = new Station();
        Train1 train1 = new Train1(station);
        Train2 train2 = new Train2(station);

        train1.start();
        train2.start();

    }
}
