package Train;

public class Train2 extends Thread{

    Station station;
    public Train2(Station station){
        this.station = station;
    }

    @Override
    public void run() {
        station.information();
    }
}
